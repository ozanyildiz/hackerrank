package algorithms.sorting;

import java.util.Scanner;

public class CountingSort2 {
	public static int[] count(int[] arr) {
		int[] c = new int[100];
		for (int i = 0; i < arr.length; i++) {
			c[arr[i]]++;
		}
		return c;
	}

	public static int[] countSort(int size, int[] countArr) {
		int[] s = new int[size];
		int k = 0;
		for (int i = 0; i < countArr.length; i++) {
			for (int j = 0; j < countArr[i]; j++) {
				s[k] = i;
				k++;
			}
		}
		return s;
	}

	public static void printArray(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int s = in.nextInt();
		int[] ar = new int[s];
		for (int i = 0; i < s; i++) {
			ar[i] = in.nextInt();
		}
		int c[] = count(ar);
		printArray(countSort(s, c));
	}
}
