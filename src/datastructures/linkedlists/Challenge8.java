package datastructures.linkedlists;

// Compare two linked lists
public class Challenge8 {
	int CompareLists(Node headA, Node headB) {
		Node currA = headA;
		Node currB = headB;
		while (currA != null) {
			if (currB == null) {
				return 0;
			}
			if (currA.data != currB.data) {
				return 0;
			}
			currA = currA.next;
			currB = currB.next;
		}
		if (currB != null) {
			return 0;
		} else {
			return 1;
		}
	}
}
