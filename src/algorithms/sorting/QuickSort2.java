package algorithms.sorting;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class QuickSort2 {

	static void quickSort(Integer[] ar) {
		if (ar == null || ar.length == 0) {
			return;
		}
		int pivot = ar[0];
		List<Integer> left = new ArrayList<>();
		List<Integer> right = new ArrayList<>();
		List<Integer> equal = new ArrayList<>();
		for (int i = 0; i < ar.length; i++) {
			if (pivot == ar[i]) {
				equal.add(ar[i]);
			} else if (ar[i] > pivot) {
				right.add(ar[i]);
			} else {
				left.add(ar[i]);
			}
		}
		quickSort(left.toArray(new Integer[left.size()]));
		System.out.print(pivot + " ");
		quickSort(left.toArray(new Integer[right.size()]));
	}

	static void printArray(Integer[] ar) {
		for (int n : ar) {
			System.out.print(n + " ");
		}
		System.out.println("");
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		Integer[] ar = new Integer[n];
		for (int i = 0; i < n; i++) {
			ar[i] = in.nextInt();
		}
		quickSort(ar);
	}
}
