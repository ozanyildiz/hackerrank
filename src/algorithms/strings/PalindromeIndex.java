package algorithms.strings;

import java.util.Scanner;

public class PalindromeIndex {

	public static boolean isPalindrom(String s) {
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != s.charAt(s.length() - 1 - i)) {
				return false;
			}
		}
		return true;
	}

	public static int palindromIndex(String s) {
		for (int i = 0; i < s.length() / 2; i++) {
			int j = s.length() - 1 - i;
			if (s.charAt(i) != s.charAt(j)) {
				if (s.charAt(i + 1) == s.charAt(j)) {
					if (isPalindrom(s.substring(0, i) + s.substring(i + 1, s.length()))) {
						return i;
					}
				}
				if (s.charAt(i) == s.charAt(j - 1)) {
					if (isPalindrom(s.substring(0, j) + s.substring(j + 1, s.length()))) {
						return j;
					}
				}
				return -1;
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t = in.nextInt();
		for (int a0 = 0; a0 < t; a0++) {
			String s = in.next();
			System.out.println(palindromIndex(s));
		}
	}
}
