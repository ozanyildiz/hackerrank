package datastructures.trees;

// Tree: Level Order Traversal
public class Challenge6 {
	void LevelOrder(Node root) {
		if (root != null) {
			java.util.Queue<Node> q = new java.util.LinkedList<>();
			q.add(root);
			while (!q.isEmpty()) {
				Node n = q.remove();
				System.out.print(n.data + " ");
				if (n.left != null) {
					q.add(n.left);
				}
				if (n.right != null) {
					q.add(n.right);
				}
			}
		}
	}
}
