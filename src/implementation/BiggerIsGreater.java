package implementation;

import java.util.Arrays;
import java.util.Scanner;

public class BiggerIsGreater {
	public static void biggerString(String s) {
        StringBuilder sb = new StringBuilder(s);
        for (int i = sb.length() - 1; i > 0; i--) {
            if (sb.charAt(i) > sb.charAt(i - 1)) {
            	char tmp = sb.charAt(i - 1);
            	sb.setCharAt(i - 1, sb.charAt(i));
            	sb.setCharAt(i, tmp);
            	System.out.println(sb.toString());
            	return;
            }
        }
        System.out.println("no answer");
    }

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		String[] a = new String[n];
		for (int a_i = 0; a_i < n; a_i++) {
			a[a_i] = in.next();
		}
		for (int i = 0; i < a.length; i++) {
			biggerString(a[i]);
		}
	}
}
