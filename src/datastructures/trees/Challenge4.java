package datastructures.trees;

// Tree: Height of a Binary Tree
public class Challenge4 {
	int height(Node root) {
		if (root == null) {
			return 0;
		}
		return 1 + java.lang.Math.max(height(root.left), height(root.right));
	}
}
