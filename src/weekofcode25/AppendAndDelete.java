package weekofcode25;

import java.util.Scanner;

public class AppendAndDelete {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.next();
		String t = in.next();
		int k = in.nextInt();

		if (s.equals(t)) {
			if (k >= s.length()) {
				System.out.println("Yes");
				return;
			}
		}

		int similar = 0;
		for (int i = 0; i < s.length(); i++) {
			if (t.length() > i && s.charAt(i) == t.charAt(i)) {
				similar++;
			} else {
				break;
			}
		}
		int opsOnS = s.substring(similar).length();
		int opsOnT = t.substring(similar).length();
		int totalOps = opsOnS + opsOnT;
		if (opsOnS < s.length()) {
			if ((k - totalOps) % 2 == 0 && (k - totalOps) >= 0) {
				System.out.println("Yes");
			} else {
				System.out.println("No");
			}
		} else {
			if ((k - totalOps) >= 0) {
				System.out.println("Yes");
			} else {
				System.out.println("No");
			}
		}
	}
}
