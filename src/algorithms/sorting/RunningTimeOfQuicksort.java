package algorithms.sorting;

import java.util.Scanner;

public class RunningTimeOfQuicksort {
	static int shif = 0;

	public static void quicksort(int[] arr, int lo, int hi) {
		if (lo < hi) {
			int p = partition(arr, lo, hi);
			quicksort(arr, lo, p - 1);
			quicksort(arr, p + 1, hi);
		}
	}

	public static int partition(int[] arr, int lo, int hi) {
		int pivot = arr[hi];
		int i = lo;
		for (int j = i; j < hi; j++) {
			if (arr[j] <= pivot) {
				swap(arr, i, j);
				i++;
			}
		}
		swap(arr, i, hi);
		return i;
	}

	public static void swap(int[] arr, int i, int j) {
		int tmp = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
		shif++;
	}

	public static int insertionSortShifts(int[] ar) {
		int shifts = 0;
		for (int i = 1; i < ar.length; i++) {
			int k = i;
			while (k > 0 && ar[k] < ar[k - 1]) {
				int tmp = ar[k];
				ar[k] = ar[k - 1];
				ar[k - 1] = tmp;
				k--;
				shifts++;
			}
		}
		return shifts;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int s = in.nextInt();
		int[] ar = new int[s];
		for (int i = 0; i < s; i++) {
			ar[i] = in.nextInt();
		}
		int[] ar2 = ar.clone();
		int sh = insertionSortShifts(ar);
		quicksort(ar2, 0, ar2.length - 1);
		System.out.println(sh - shif);
	}
}
