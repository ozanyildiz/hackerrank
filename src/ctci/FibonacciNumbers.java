package ctci;

import java.util.Scanner;

public class FibonacciNumbers {

	public static int fibonacciHelper(int n, int first, int second) {
		if (n == 0) {
			return first;
		}
		return fibonacciHelper(--n, second, first + second);
	}

	public static int fibonacci(int n) {
		return fibonacciHelper(n, 0, 1);
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		scanner.close();
		System.out.println(fibonacci(n));
	}
}
