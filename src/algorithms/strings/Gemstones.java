package algorithms.strings;

import java.util.Scanner;

public class Gemstones {

	public static int count(String[] s) {
		boolean[] map = new boolean[127];
		int count = 0;
		String ref = s[0];
		for (int i = 0; i < ref.length(); i++) {
			char refChar = ref.charAt(i);
			boolean contain = true;
			if (!map[refChar]) {
				map[refChar] = true;
				for (int j = 1; j < s.length && contain; j++) {
					contain = contain && s[j].contains(String.valueOf(refChar));
				}
				if (contain) {
					count++;
				}
			}
		}

		return count;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		String[] s = new String[n];
		for (int i = 0; i < s.length; i++) {
			s[i] = in.next();
		}
		System.out.println(count(s));
	}

}
