package implementation;

import java.util.Scanner;

public class JumpingOnTheClouds {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int a[] = new int[n];
		for (int a_i = 0; a_i < n; a_i++) {
			a[a_i] = in.nextInt();
		}
		int i = 0;
		int count = 0;
		while (i < a.length - 2) {
			count++;
			if (a[i + 2] == 0) {
				i += 2;
			} else {
				i += 1;
			}
		}
		System.out.println(++count);
	}
}
