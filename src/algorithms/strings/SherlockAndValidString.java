package algorithms.strings;

import java.util.Arrays;
import java.util.Scanner;

public class SherlockAndValidString {

	public static int findStart(int[] sorted) {
		for (int i = 0; i < sorted.length; i++) {
			if (sorted[i] != 0) {
				return i;
			}
		}
		return -1;
	}

	public static boolean isValid(int[] letterCount) {
		Arrays.sort(letterCount);
		int start = findStart(letterCount);
		boolean first = true;
		boolean second = true;
		for (int i = start + 1; i < letterCount.length - 1; i++) {
			if (letterCount[i] != letterCount[i + 1]) {
				first = false;
				break;
			}
		}
		for (int i = start; i < letterCount.length - 2; i++) {
			if (letterCount[i] != letterCount[i + 1]) {
				second = false;
				break;
			}
		}
		return first || second;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.next();
		int[] letterCount = new int[26];
		for (int i = 0; i < s.length(); i++) {
			int j = s.charAt(i) - 'a';
			letterCount[j]++;
		}
		if (isValid(letterCount)) {
			System.out.println("YES");
		} else {
			System.out.println("NO");
		}
	}
}
