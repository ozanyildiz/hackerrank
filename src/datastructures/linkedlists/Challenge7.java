package datastructures.linkedlists;

// Reverse a linked list
public class Challenge7 {
	Node Reverse(Node head) {
		if (head == null) {
			return null;
		}
		Node reversed = null;
		Node curr = head;
		while (curr != null) {
			Node temp = reversed;
			reversed = new Node();
			reversed.data = curr.data;
			reversed.next = temp;
			curr = curr.next;
		}
		return reversed;
	}
}
