package algorithms.sorting;

import java.util.Scanner;

public class QuickSortInPlace {
	public static void quicksort(int[] arr, int lo, int hi) {
		if (lo < hi) {
			int p = partition(arr, lo, hi);
			quicksort(arr, lo, p - 1);
			quicksort(arr, p + 1, hi);
		}
	}

	public static int partition(int[] arr, int lo, int hi) {
		int pivot = arr[hi];
		int i = lo;
		for (int j = i; j < hi; j++) {
			if (arr[j] <= pivot) {
				swap(arr, i, j);
				i++;
			}
		}
		swap(arr, i, hi);
		printArray(arr);
		return i;
	}

	public static void swap(int[] arr, int i, int j) {
		int tmp = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
	}

	static void printArray(int[] ar) {
		for (int n : ar) {
			System.out.print(n + " ");
		}
		System.out.println("");
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] ar = new int[n];
		for (int i = 0; i < n; i++) {
			ar[i] = in.nextInt();
		}
		quicksort(ar, 0, ar.length - 1);
	}
}
