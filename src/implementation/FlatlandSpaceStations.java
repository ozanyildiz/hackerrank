package implementation;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class FlatlandSpaceStations {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int m = in.nextInt();
		int c[] = new int[m];
		for (int c_i = 0; c_i < m; c_i++) {
			c[c_i] = in.nextInt();
		}

		int map[] = new int[n];
		Arrays.fill(map, n);
		for (int i = 0; i < c.length; i++) {
			for (int j = 0; j < map.length; j++) {
				if (c[i] == j)
					map[j] = 0;
				else {
					int cost = Math.abs(c[i] - j);
					if (cost < map[j]) {
						map[j] = cost;
					}
				}
			}
		}

		int max = 0;
		for (int i = 0; i < map.length; i++) {
			if (max < map[i]) {
				max = map[i];
			}
		}
		System.out.println(max);
	}
}
