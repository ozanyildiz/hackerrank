package algorithms.strings;

import java.util.Scanner;

public class LoveLetterMistery {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		for (int i = 0; i < n; i++) {
			String s = in.next();
			int c = 0;
			for (int j = 0; j < s.length() / 2; j++) {
				c += Math.abs(s.charAt(j) - s.charAt(s.length() - j - 1));
			}
			System.out.println(c);
		}
	}
}
