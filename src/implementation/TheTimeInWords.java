package implementation;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class TheTimeInWords {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int h = in.nextInt();
		int m = in.nextInt();

		Map<Integer, String> hourDesc = new HashMap<Integer, String>();
		hourDesc.put(1, "one");
		hourDesc.put(2, "two");
		hourDesc.put(3, "three");
		hourDesc.put(4, "four");
		hourDesc.put(5, "five");
		hourDesc.put(6, "six");
		hourDesc.put(7, "seven");
		hourDesc.put(8, "eight");
		hourDesc.put(9, "nine");
		hourDesc.put(10, "ten");
		hourDesc.put(11, "eleven");
		hourDesc.put(12, "twelve");
		hourDesc.put(13, "thirteen");
		hourDesc.put(14, "fourteen");
		hourDesc.put(16, "sixteen");
		hourDesc.put(17, "seventeen");
		hourDesc.put(18, "eighteen");
		hourDesc.put(19, "nineteen");
		hourDesc.put(20, "twenty");
		hourDesc.put(21, "twenty one");
		hourDesc.put(22, "twenty two");
		hourDesc.put(23, "twenty three");
		hourDesc.put(24, "twenty four");
		hourDesc.put(25, "twenty five");
		hourDesc.put(26, "twenty six");
		hourDesc.put(27, "twenty seven");
		hourDesc.put(28, "twenty eight");
		hourDesc.put(29, "twenty nine");

		if (m == 0) {
			System.out.println(hourDesc.get(h) + " o'clock");
		} else if (m == 1) {
			System.out.println(hourDesc.get(m) + " minute past " + hourDesc.get(h));
		} else if (m == 15) {
			System.out.println("quarter past " + hourDesc.get(h));
		} else if (m > 1 && m < 30) {
			System.out.println(hourDesc.get(m) + " minutes past " + hourDesc.get(h));
		} else if (m == 30) {
			System.out.println("half past " + hourDesc.get(h));
		} else if (m == 45) {
			System.out.println("quarter to " + hourDesc.get((h + 1) % 12));
		} else if (m == 59) {
			System.out.println(hourDesc.get(60 - m) + " minute to " + hourDesc.get((h + 1) % 12));
		} else {
			System.out.println(hourDesc.get(60 - m) + " minutes to " + hourDesc.get((h + 1) % 12));
		}
	}
}
