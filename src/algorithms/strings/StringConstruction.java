package algorithms.strings;

import java.util.Scanner;

public class StringConstruction {

	public static int costOfBuildString(String s) {
		String p = "";
		int cost = 0;
		for (int i = 0; i < s.length(); i++) {
			String c = String.valueOf(s.charAt(i));
			if (!p.contains(c)) {
				p += c;
				cost++;
			}
		}
		return cost;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		for (int a0 = 0; a0 < n; a0++) {
			String s = in.next();
			System.out.println(costOfBuildString(s));
		}
	}

}
