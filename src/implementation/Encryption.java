package implementation;

import java.util.Scanner;

public class Encryption {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.next();

		int lengthWithoutSpace = s.replace(" ", "").length();
		int row = Double.valueOf(Math.floor(Math.sqrt(lengthWithoutSpace))).intValue();
		int column = Double.valueOf(Math.ceil(Math.sqrt(lengthWithoutSpace))).intValue();

		if (row * column < lengthWithoutSpace) {
			row = column;
		}

		String[] sarr = new String[row];
		int k = 0;
		for (int i = 0; i < s.length(); i += column) {
			if (i + column > s.length()) {
				sarr[k] = s.substring(i, s.length());
			} else {
				sarr[k] = s.substring(i, i + column);
			}
			k++;
		}

		for (int i = 0; i < column; i++) {
			for (int j = 0; j < row; j++) {
				if (i < sarr[j].length())
					System.out.print(sarr[j].charAt(i));
			}
			System.out.print(" ");
		}

	}

}
