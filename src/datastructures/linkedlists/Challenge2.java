package datastructures.linkedlists;

//Insert a Node at the Tail of a Linked List
public class Challenge2 {

	Node Insert(Node head, int data) {
		Node node = new Node();
		node.data = data;
		if (head == null) {
			return node;
		}
		Node curr = head;
		while (curr.next != null) {
			curr = curr.next;
		}
		curr.next = node;
		return head;
	}
}
