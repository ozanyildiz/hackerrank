package implementation;

import java.util.Scanner;

public class SherlockAndSquares {
	public static int findSquareNumbers(int lower, int upper) {
		int count = 0;
		for (int i = lower; i <= upper;) {
			if (Math.ceil(Math.sqrt(i)) == Math.floor(Math.sqrt(i))) {
				count++;
				i = Double.valueOf(Math.sqrt(i)).intValue() + 1;
				i = i * i;
			} else {
				i++;
			}
		}
		return count;
	}

	public static int findSquareNumbers2(int lower, int upper) {
		int f = Double.valueOf(Math.sqrt(lower)).intValue();
		int s = Double.valueOf(Math.sqrt(upper)).intValue();
		if (f * f == lower) {
			return s - f + 1;
		} else {
			return s - f;
		}

	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t = in.nextInt();
		for (int a0 = 0; a0 < t; a0++) {
			int n = in.nextInt();
			int m = in.nextInt();
			System.out.println(findSquareNumbers(n, m));
		}
	}
}
