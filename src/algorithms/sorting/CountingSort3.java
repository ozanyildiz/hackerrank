package algorithms.sorting;

import java.util.Scanner;

public class CountingSort3 {
	public static int[] count(int[] arr) {
		int[] c = new int[100];
		for (int i = 0; i < arr.length; i++) {
			c[arr[i]]++;
		}
		return c;
	}

	public static int[] count2(int[] arr) {
		for (int i = 1; i < arr.length; i++) {
			arr[i] += arr[i - 1];
		}
		return arr;
	}

	public static void printArray(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int s = in.nextInt();
		int[] ar = new int[s];
		for (int i = 0; i < s; i++) {
			ar[i] = in.nextInt();
			String p = in.next();
		}
		int c[] = count(ar);
		printArray(count2(c));
	}
}
