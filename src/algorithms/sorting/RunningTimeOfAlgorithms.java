package algorithms.sorting;

import java.util.Scanner;

public class RunningTimeOfAlgorithms {
	public static void insertionSortShifts(int[] ar) {
		int shifts = 0;
		for (int i = 1; i < ar.length; i++) {
			int k = i;
			while (k > 0 && ar[k] < ar[k - 1]) {
				int tmp = ar[k];
				ar[k] = ar[k - 1];
				ar[k - 1] = tmp;
				k--;
				shifts++;
			}
		}
		System.out.println(shifts);
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int s = in.nextInt();
		int[] ar = new int[s];
		for (int i = 0; i < s; i++) {
			ar[i] = in.nextInt();
		}
		insertionSortShifts(ar);

	}

}
