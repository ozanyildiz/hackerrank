package ctci;

import java.util.HashMap;
import java.util.Scanner;

public class DavisStaircase {
	
    public static HashMap<Integer, Integer> hmap = new HashMap<>();
    
	public static int moves(int n) {
		if (n == 0) {
			return 0;
		}
		else if (n == 1) {
			return 1;
		}
		else if (n == 2) {
			return 2;
		}
		else if (n == 3) {
			return 4;
		}
        if (hmap.containsKey(n)) {
            return hmap.get(n);
        } else {
            int r = moves(n - 3) + moves(n - 2) + moves (n - 1);
            hmap.put(n, r);
            return r;
        }
	}
	
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int s = in.nextInt();
        for(int a0 = 0; a0 < s; a0++){
            int n = in.nextInt();
            System.out.println(moves(n));
        }
    }
}
