package implementation;

import java.math.BigInteger;
import java.util.Scanner;

public class ExtraLongFactorials {
	
	public static BigInteger factorial(BigInteger n) {
		if (n.equals(BigInteger.ONE)) {
			return BigInteger.ONE;
		}
		return n.multiply(factorial(n.subtract(BigInteger.ONE)));
	}
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		System.out.print(factorial(BigInteger.valueOf(n)));
	}
}
