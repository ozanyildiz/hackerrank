package algorithms.strings;

import java.util.Scanner;

public class MarsExploration {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.next();

		String w = "SOS";

		int count = 0;
		for (int i = 0; i <= s.length() - 3; i += 3) {
			for (int j = 0; j < w.length(); j++)
				if (s.charAt(i + j) != w.charAt(j)) {
					count++;
				}
		}
		System.out.println(count);
	}
}
