package implementation;

import java.util.Scanner;

public class StrangeCounter {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		long t = in.nextLong();
		int c = 1;
		long i = 3;
		while (t > i) {
			i = i + 3 * Double.valueOf(Math.pow(2, c)).longValue();
			c++;
		}
		System.out.println(i - t + 1);
	}
}
