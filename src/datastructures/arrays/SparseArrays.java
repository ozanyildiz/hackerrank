package datastructures.arrays;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SparseArrays {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		Map<String, Integer> map = new HashMap<>();
		for (int i = 0; i < n; i++) {
			String key = in.next();
			if (map.containsKey(key)) {
				map.put(key, map.get(key) + 1);
			} else {
				map.put(key, 1);
			}

		}
		int m = in.nextInt();
		for (int i = 0; i < m; i++) {
			String check = in.next();
			if (map.containsKey(check)) {
				System.out.println(map.get(check));
			} else {
				System.out.println(0);
			}
		}
	}

}
