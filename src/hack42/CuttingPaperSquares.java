package hack42;

import java.util.Scanner;

public class CuttingPaperSquares {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long n = in.nextInt();
        long m = in.nextInt();
        System.out.println(n * m - 1);
    }

}
