package implementation;

import java.util.Scanner;

public class FindDigits {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t = in.nextInt();
		for (int a0 = 0; a0 < t; a0++) {
			int n = in.nextInt();
			int x = n;
			int count = 0;
			while (x > 0) {
				int y = x % 10;
				if (y != 0 && n % y == 0) {
					count++;
				}
				x = x / 10;
			}
			System.out.println(count);
		}
	}

}
