package algorithms.strings;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class GameOfThronesI {
	public static boolean canBePalindrom(String s) {
		Map<Character, Integer> letterCount = new HashMap<>();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (letterCount.containsKey(c)) {
				letterCount.put(c, letterCount.get(c) + 1);
			} else {
				letterCount.put(c, 1);
			}
		}
		int sum = 0;
		for (Map.Entry<Character, Integer> entry : letterCount.entrySet()) {
			sum += entry.getValue() % 2;
		}
		if (sum >= 2) {
			return false;
		} else {
			return true;
		}
	}

	public static void main(String[] args) {
		Scanner myScan = new Scanner(System.in);
		String inputString = myScan.nextLine();

		if (canBePalindrom(inputString)) {
			System.out.println("YES");
		} else {
			System.out.println("NO");
		}
		myScan.close();
	}
}
