package implementation;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class SockMerchant {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int c[] = new int[n];
		for (int c_i = 0; c_i < n; c_i++) {
			c[c_i] = in.nextInt();
		}

		int count = 0;
		Set<Integer> set = new HashSet<>();
		for (int i = 0; i < c.length; i++) {
			if (set.contains(c[i])) {
				set.remove(c[i]);
				count++;
			} else {
				set.add(c[i]);
			}
		}
		System.out.println(count);
	}
}
