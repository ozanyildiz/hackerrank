package algorithms.sorting;

import java.util.Scanner;

public class Intro {

	public static int findIndex(int[] nums, int v) {
		int i = 0;
		while (v > nums[i]) {
			i++;
		}
		return i;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int v = in.nextInt();
		int n = in.nextInt();

		int[] numbers = new int[n];
		for (int i = 0; i < n; i++) {
			int j = in.nextInt();
			numbers[i] = j;
		}
		System.out.println(findIndex(numbers, v));
	}
}
