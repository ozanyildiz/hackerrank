package ctci;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class RansomNote {
	Map<String, Integer> magazineMap;
	String[] noteArray;

	public RansomNote(String magazine, String note) {
		String[] s1 = magazine.split(" ");
		this.magazineMap = new HashMap<>();
		for (int i = 0; i < s1.length; i++) {
			if (magazineMap.containsKey(s1[i])) {
				magazineMap.put(s1[i], magazineMap.get(s1[i]) + 1);
			} else {
				magazineMap.put(s1[i], 1);
			}
		}
		this.noteArray = note.split(" ");
	}

	public boolean solve() {
		for (int i = 0; i < noteArray.length; i++) {
			if (magazineMap.containsKey(noteArray[i]) && magazineMap.get(noteArray[i]) > 0) {
				magazineMap.put(noteArray[i], magazineMap.get(noteArray[i]) - 1);
			} else {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int m = scanner.nextInt();
		int n = scanner.nextInt();

		// Eat whitespace to beginning of next line
		scanner.nextLine();

		RansomNote s = new RansomNote(scanner.nextLine(), scanner.nextLine());
		scanner.close();

		boolean answer = s.solve();
		if (answer)
			System.out.println("Yes");
		else
			System.out.println("No");

	}
}
