package implementation;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class HappyLadybugs {
	public static boolean happyLadybug(String b) {
		int[] letters = new int[26];
		boolean emptySlot = false;
		for (int i = 0; i < b.length(); i++) {
			if (b.charAt(i) == '_') {
				emptySlot = true;
			} else {
				int j = b.charAt(i) - 'A';
				letters[j]++;
			}
		}
		for (int i = 0; i < letters.length; i++) {
			if (letters[i] == 1) {
				return false;
			}
		}
		if (emptySlot) {
			return true;
		} else {
			Set<Character> passed = new HashSet<>();
			for (int i = 0; i < b.length() - 1; i++) {
				if (passed.contains(b.charAt(i))) {
					return false;
				} else if (b.charAt(i) != b.charAt(i + 1)) {
					passed.add(b.charAt(i));
				}
			}
		}
		return true;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int Q = in.nextInt();
		for (int a0 = 0; a0 < Q; a0++) {
			int n = in.nextInt();
			String b = in.next();
			if (happyLadybug(b)) {
				System.out.println("YES");
			} else {
				System.out.println("NO");
			}
		}
	}
}
