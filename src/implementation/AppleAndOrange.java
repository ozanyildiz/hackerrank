package implementation;

import java.util.Scanner;

public class AppleAndOrange {
	public static int fruitCount(int[] fruit, int s, int t, int pos) {
		int count = 0;
		for (int i = 0; i < fruit.length; i++) {
			if (pos + fruit[i] >= s && pos + fruit[i] <= t) {
				count++;
			}
		}
		return count;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int s = in.nextInt();
		int t = in.nextInt();
		int a = in.nextInt();
		int b = in.nextInt();
		int m = in.nextInt();
		int n = in.nextInt();
		int[] apple = new int[m];
		for (int apple_i = 0; apple_i < m; apple_i++) {
			apple[apple_i] = in.nextInt();
		}
		int[] orange = new int[n];
		for (int orange_i = 0; orange_i < n; orange_i++) {
			orange[orange_i] = in.nextInt();
		}

		System.out.println(fruitCount(apple, s, t, a));
		System.out.println(fruitCount(orange, s, t, b));
	}
}
