package algorithms.strings;

import java.util.Scanner;

public class CamelCase {

	public static int countWords(String s) {
		if (s == null) {
			return 0;
		}
		String uppercaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		int count = 0;
		for (int i = 0; i < s.length(); i++) {
			if (i == 0 && uppercaseLetters.toLowerCase().contains(String.valueOf(s.charAt(i)))) {
				count++;
			}
			else if (uppercaseLetters.contains(String.valueOf(s.charAt(i)))) {
				count++;
			}
		}
		return count;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.next();
		System.out.println(countWords(s));
	}
}
