package implementation;

import java.util.Scanner;

public class UtopianTree {
	public static int calcHeight(int n) {
		int h = 1;
		for (int i = 0; i < n; i++) {
			if (i % 2 == 0) {
				h *= 2;
			} else {
				h++;
			}
		}
		return h;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t = in.nextInt();
		for (int a0 = 0; a0 < t; a0++) {
			int n = in.nextInt();
			System.out.println(calcHeight(n));
		}
	}
}
