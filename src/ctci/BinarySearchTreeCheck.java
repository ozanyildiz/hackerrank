package ctci;

import datastructures.trees.Node;

public class BinarySearchTreeCheck {
	boolean greaterThanBad(int data, Node root) {
		if (root == null)
			return true;
		if (root.data <= data) {
			return false;
		} else {
			return greaterThanBad(data, root.left) && greaterThanBad(data, root.right);
		}
	}

	boolean lessThanBad(int data, Node root) {
		if (root == null)
			return true;
		if (root.data >= data) {
			return false;
		} else {
			return lessThanBad(data, root.left) && lessThanBad(data, root.right);
		}
	}

	boolean checkBSTBad(Node root) {
		if (root == null)
			return true;
		else {
			return greaterThanBad(root.data, root.right) && lessThanBad(root.data, root.left) && checkBSTBad(root.left)
					&& checkBSTBad(root.right);
		}
	}

	boolean checkBST(Node root, int min, int max) {
		if (root == null)
			return true;
		if (root.data <= min || root.data >= max) {
			return false;
		} else {
			return checkBST(root.left, min, root.data) && checkBST(root.right, root.data, max);
		}
	}

	boolean checkBST(Node root) {
		return checkBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
}
