package implementation;

import java.util.Scanner;

public class BonAppetit {
	
	public static int fairSplit(int[] p, int c, int k) {
		int t = 0;
		for (int i = 0; i < p.length; i++) {
			if (i == k) {
				continue;
			}
			t += p[i];
		}
		return c - t / 2;
	}
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int k = in.nextInt();
		int[] p = new int[n];
		for (int i = 0; i < n; i++) {
			p[i] = in.nextInt();
		}
		int c = in.nextInt();
		int m = fairSplit(p, c, k);
		if (m == 0) {
			System.out.println("Bon Appetit");
		} else {
			System.out.println(m);
		}
	}
}
