package implementation;

import java.util.Scanner;

public class RepeatedString {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.next();
		long n = in.nextLong();
		int a = s.length() - s.replace("a", "").length();
		long c = (n / s.length()) * a;
		long r = n - (s.length() * (n / s.length()));
		for (int i = 0; i < r; i++) {
			if (s.charAt(i) == 'a') {
				c++;
			}
		}
		System.out.println(c);
	}

}
