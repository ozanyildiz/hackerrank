package implementation;

import java.util.Scanner;

public class CutTheSticks {
	public static int findMin(int[] a) {
		int min = 1001;
		for (int i = 0; i < a.length; i++) {
			if (a[i] != 0 && a[i] < min) {
				min = a[i];
			}
		}
		return min;
	}

	public static void cutSticks(int[] a) {
		while (true) {
			int min = findMin(a);
			if (min == 1001) {
				return;
			}
			int count = 0;
			for (int i = 0; i < a.length; i++) {
				if (a[i] != 0) {
					a[i] -= min;
					count++;
				}
			}
			System.out.println(count);
		}
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int a[] = new int[n];
		for (int i = 0; i < n; i++) {
			a[i] = in.nextInt();
		}
		cutSticks(a);
	}
}
