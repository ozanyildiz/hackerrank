package ctci;

import java.util.HashSet;
import java.util.Set;

import datastructures.linkedlists.Node;

public class DetectACycle {
	boolean hasCycle(Node head) {
		Node curr = head;
		Set<Node> visited = new HashSet<>();
		while (curr != null) {
			if (visited.contains(curr)) {
				return true;
			}
			visited.add(curr);
			curr = curr.next;
		}
		return false;
	}

	boolean hasCycle2(Node head) {
		Node fast = head.next;
		Node slow = head;
		while (fast != null && fast.next != null && slow != null) {
			if (fast == slow) {
				return true;
			}
			fast = fast.next.next;
			slow = slow.next;
		}
		return false;
	}
}
