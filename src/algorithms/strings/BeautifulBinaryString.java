package algorithms.strings;

import java.util.Scanner;

public class BeautifulBinaryString {

	public static int count(String s) {
		if (s == null) {
			return 0;
		}
		int count = 0;
		while(s.contains("010")) {
			s = s.replaceFirst("010", "011");
			count++;
		}
		return count;
	}
	
	public static void main2(String[] args) {
		System.out.print(count("001010"));
	}

}
