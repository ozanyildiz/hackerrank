package implementation;

import java.util.Scanner;

public class ACMICPCTeam {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		int M = in.nextInt();

		String[] a = new String[N];

		for (int i = 0; i < N; i++) {
			a[i] = in.next();
		}

		int max = 0;
		int team = 0;
		for (int i = 0; i < N - 1; i++) {
			for (int j = i + 1; j < N; j++) {
				int currentMax = 0;
				for (int k = 0; k < M; k++) {
					if (a[i].charAt(k) == '1' || a[j].charAt(k) == '1') {
						currentMax++;
					}
				}
				if (currentMax > max) {
					max = currentMax;
					team = 1;
				} else if (currentMax == max) {
					team++;
				}
			}
		}
		System.out.println(max);
		System.out.println(team);
	}
}
