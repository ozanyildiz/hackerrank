package ctci;

import java.util.Scanner;

public class MakingAnagrams {
	public static int NUM_LETTERS = 26;

	public static int[] getCharCounts(String s) {
		int[] charCounts = new int[NUM_LETTERS];
		int offset = (int) 'a';
		for (int i = 0; i < s.length(); i++) {
			charCounts[s.charAt(i) - offset]++;
		}
		return charCounts;
	}

	public static int numberNeeded(String first, String second) {
		int[] firstArray = getCharCounts(first);
		int[] secondArray = getCharCounts(second);

		int diff = 0;
		for (int i = 0; i < NUM_LETTERS; i++) {
			if (firstArray[i] != secondArray[i]) {
				diff += Math.abs(firstArray[i] - secondArray[i]);
			}
		}
		return diff;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String a = in.next();
		String b = in.next();
		System.out.println(numberNeeded(a, b));
	}
}
