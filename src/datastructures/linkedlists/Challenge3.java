package datastructures.linkedlists;

// Insert a node at the head of a linked list
public class Challenge3 {
	Node Insert(Node head, int x) {
		Node node = new Node();
		node.data = x;
		if (head == null) {
			return node;
		}
		node.next = head;
		return node;
	}
}
