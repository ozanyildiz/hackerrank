package datastructures.linkedlists;

// Print the Elements of a Linked List
public class Challenge1 {

	void Print(Node head) {
		Node curr = head;
		while (curr != null) {
			System.out.println(curr.data);
			curr = curr.next;
		}
	}
}
