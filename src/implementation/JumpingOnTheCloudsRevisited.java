package implementation;

import java.util.Scanner;

public class JumpingOnTheCloudsRevisited {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int k = in.nextInt();
		int c[] = new int[n];
		for (int c_i = 0; c_i < n; c_i++) {
			c[c_i] = in.nextInt();
		}

		int energy = 100;
		boolean firstPass = true;
		for (int i = 0; i % c.length != 0 || firstPass; i += k) {
			if (firstPass) {
				firstPass = false;
			}
			energy--;
			if (c[i] == 1) {
				energy -= 2;
			}
		}
		System.out.println(energy);
	}
}
