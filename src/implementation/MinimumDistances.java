package implementation;

import java.util.*;

public class MinimumDistances {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int a[] = new int[n];
		for (int A_i = 0; A_i < n; A_i++) {
			a[A_i] = in.nextInt();
		}
		int max = 10000000;
		int nearest = max;
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (a[i] == a[j]) {
					int curr = Math.abs(i - j);
					if (curr < nearest) {
						nearest = curr;
					}
				}
			}
		}
		if (nearest == max) {
			System.out.println(-1);
		} else {
			System.out.println(nearest);
		}
	}
}
