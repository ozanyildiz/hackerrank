package ctci;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

public class BalancedBrackets {

	public static boolean isBalanced(String expression) {
		Map<String, String> parens = new HashMap<>();
		parens.put("{", "}");
		parens.put("[", "]");
		parens.put("(", ")");

		Stack<String> stack = new Stack<>();
		for (int i = 0; i < expression.length(); i++) {
			String e = String.valueOf(expression.charAt(i));
			if (parens.containsKey(e)) {
				stack.push(e);
			} else {
				if (stack.isEmpty())
					return false;
				String c = stack.pop();
				if (!parens.get(c).equals(e)) {
					return false;
				}
			}
		}
		return stack.isEmpty();
	}

	public static boolean isBalanced2(String expression) {
		Stack<Character> stack = new Stack<>();
		for (int i = 0; i < expression.length(); i++) {
			char curr = expression.charAt(i);
			if (curr == '}') {
				if (stack.isEmpty() || stack.pop() != '{')
					return false;
			} else if (curr == ']') {
				if (stack.isEmpty() || stack.pop() != '[')
					return false;
			} else if (curr == ')') {
				if (stack.isEmpty() || stack.pop() != '(')
					return false;
			} else {
				stack.push(curr);
			}
		}
		return stack.isEmpty();
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t = in.nextInt();
		for (int a0 = 0; a0 < t; a0++) {
			String expression = in.next();
			boolean answer = isBalanced(expression);
			if (answer)
				System.out.println("YES");
			else
				System.out.println("NO");
		}
	}
}
