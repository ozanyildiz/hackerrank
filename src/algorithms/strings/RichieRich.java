package algorithms.strings;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class RichieRich {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int z = in.nextInt();
		int d = in.nextInt();
		String number = in.next();
		StringBuilder n = new StringBuilder(number);
		int c = 0;
		Set<Integer> set = new HashSet<>();
		for (int i = 0; i < n.length() / 2; i++) {
			int j = n.length() - i - 1;
			if (n.charAt(i) != n.charAt(j)) {
				c++;
				set.add(i);
				if (n.charAt(i) > n.charAt(j)) {
					n.setCharAt(j, n.charAt(i));
				} else {
					n.setCharAt(i, n.charAt(j));
				}
			}
		}
		int t = c;
		if (c > d) {
			System.out.println(-1);
		} else {
			for (int i = 0; i < d - c; i++) {
				for (int j = 0; j < n.length() / 2; j++) {
					if (n.charAt(j) == '9') {
						continue;
					} else {
						if (set.contains(j)) {
							n.setCharAt(j, '9');
							n.setCharAt(n.length() - 1 - j, '9');
							t++;
						} else {
							if (i + 2 <= d - c) {
								n.setCharAt(j, '9');
								n.setCharAt(n.length() - 1 - j, '9');
								t += 2;
								i++;
							}
						}
						break;
					}
				}
			}
			if (n.length() % 2 == 1 && d - t > 0) {
				n.setCharAt(n.length() / 2, '9');
			}
			System.out.println(n.toString());
		}
	}
}
