package algorithms.sorting;

import java.util.Scanner;

public class InsertionSortPart2 {
	public static void insertionSortPart2(int[] ar) {
		for (int i = 1; i < ar.length; i++) {
			int k = i;
			while (k > 0 && ar[k] < ar[k - 1]) {
				int tmp = ar[k];
				ar[k] = ar[k - 1];
				ar[k - 1] = tmp;
				k--;
			}
			printArray(ar);
		}
	}

	public static void insert(int[] ar, int num) {
		int i = ar.length - 2;
		while (i >= 0 && num < ar[i]) {
			ar[i + 1] = ar[i];
			printArray(ar);
			i--;
		}
		ar[i + 1] = num;
		printArray(ar);
	}
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int s = in.nextInt();
		int[] ar = new int[s];
		for (int i = 0; i < s; i++) {
			ar[i] = in.nextInt();
		}
		insertionSortPart2(ar);

	}

	private static void printArray(int[] ar) {
		for (int n : ar) {
			System.out.print(n + " ");
		}
		System.out.println("");
	}

}
