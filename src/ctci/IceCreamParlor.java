package ctci;

import java.util.Arrays;
import java.util.Scanner;

public class IceCreamParlor {

	static class IceCream implements Comparable {
		int cost;
		int index;

		public IceCream(int cost, int index) {
			this.cost = cost;
			this.index = index;
		}

		@Override
		public int compareTo(Object o) {
			return this.cost - ((IceCream) o).cost;
		}

		@Override
		public boolean equals(Object o) {
			return this.cost == ((IceCream) o).cost;
		}

	}

	public static int binarySearch(int first, int last, IceCream[] arr, int search) {
		for (int i = first; i < last; i++) {
			if (arr[i].cost == search) {
				return arr[i].index;
			}
		}
		return -1;
	}

	public static void main(String[] args) {

		int t;
		int n, m;

		Scanner in = new Scanner(System.in);
		t = in.nextInt();
		for (int test = 0; test < t; test++) {

			m = in.nextInt();
			n = in.nextInt();
			IceCream[] arr = new IceCream[n];

			for (int i = 0; i < n; i++)
				arr[i] = new IceCream(in.nextInt(), i + 1);

			Arrays.sort(arr);
			int firstIndex = 100000, secondIndex = 100000;
			for (int i = 0; i < n; i++) {
				int search = m - arr[i].cost;
				if (search >= arr[i].cost) {
					int index = binarySearch(i + 1, n, arr, search);
					if (index != -1) {
						System.out.println(Math.min(arr[i].index, index) + " " + Math.max(arr[i].index, index));
						break;

					}
				}
			}

		}

	}

}
