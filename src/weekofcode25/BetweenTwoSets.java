package weekofcode25;

import java.util.Scanner;

public class BetweenTwoSets {
    public static boolean isAllFactorOfX(int x, int[] a) {
        for (int i = 0; i < a.length; i++) {
            if (x % a[i] != 0) {
                return false;
            }
        }
        return true;
    }
    
    public static boolean isXFactorOfAll(int x, int[] a) {
        for (int i = 0; i < a.length; i++) {
            if (a[i] % x != 0) {
                return false;
            }
        }
        return true;
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        int[] a = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
        int[] b = new int[m];
        for(int b_i=0; b_i < m; b_i++){
            b[b_i] = in.nextInt();
        }
      
        int count = 0;
        for (int i = 1; i <= 100; i++) {
            if (isAllFactorOfX(i, a) && isXFactorOfAll(i, b)) {
                count++;
            }
        }
        System.out.println(count);
    }
}
