package algorithms.strings;

import java.util.Scanner;

public class AlternatingCharacters {

	public static int count(String s) {
		if (s == null) {
			return 0;
		}
		int count = 0;
		for (int i = 0; i < s.length() - 1; i++) {
			if (s.charAt(i) == s.charAt(i + 1)) {
				count++;
			}
		}
		return count;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int j = in.nextInt();
		for (int i = 0; i < j; i++) {
			String s = in.next();
			System.out.println(count(s));
		}
	}

}
