package algorithms.strings;

import java.util.Scanner;

public class CaesarCipher {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		String s = in.next();
		int k = in.nextInt();
        int shift = k % 26;
        for (int i = 0; i < s.length(); i++) {
            char curr = s.charAt(i);
            if (curr >= 'a' && curr <= 'z') {
            	char shifted = (char) (curr + shift);
            	if (shifted > 'z') {
            		System.out.print((char) ('a' - 1 + shifted % 'z'));
            	} else {
            		System.out.print(shifted);
            	}
            } else if (curr >= 'A' && curr <= 'Z') {
            	char shifted = (char) (curr + shift);
            	if (shifted > 'Z') {
            		System.out.print((char) ('A' - 1 + shifted % 'Z'));
            	} else {
            		System.out.print(shifted);
            	}
            } else {
                System.out.print(curr);
            }
        }
	}
}
