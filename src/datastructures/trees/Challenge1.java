package datastructures.trees;

// Tree: Inorder Traversal
public class Challenge1 {
	void inOrder(Node root) {
		if (root != null) {
			inOrder(root.left);
			System.out.print(root.data + " ");
			inOrder(root.right);
		}
	}
}
