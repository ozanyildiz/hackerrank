package datastructures.linkedlists;

// Delete a Node
public class Challenge5 {
	Node Delete(Node head, int position) {
		if (head == null) {
			return null;
		}
		if (position == 0) {
			return head.next;
		}
		Node curr = head;
		for (int i = 0; i < position - 1; i++) {
			curr = curr.next;
		}
		curr.next = curr.next.next;
		return head;
	}
}
