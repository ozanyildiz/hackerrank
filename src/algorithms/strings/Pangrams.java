package algorithms.strings;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Pangrams {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		String t = s.replace(" ", "").toLowerCase();

		Set<Character> letters = new HashSet<>();
		for (int i = 0; i < t.length(); i++) {
			letters.add(t.charAt(i));
		}
		if (letters.size() == 26) {
			System.out.print("pangram");
		} else {
			System.out.print("not pangram");
		}
	}
}