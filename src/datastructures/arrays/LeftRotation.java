package datastructures.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class LeftRotation {

	public static void shiftLeft(int[] a, int d) {
		int[] b = new int[a.length];
		for (int i = 0; i < a.length; i++) {
			int j = i - d;
			if (j < 0) {
				j += a.length;
			}
			b[j] = a[i];
		}
		for (int i = 0; i < b.length; i++) {
			System.out.print(b[i] + " ");
		}
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int d = in.nextInt();
		int[] a = new int[n];
		for (int i = 0; i < n; i++) {
			a[i] = in.nextInt();
		}
		shiftLeft(a, d);
	}

}
