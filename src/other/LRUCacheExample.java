package other;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class LRUCacheExample {

	public static class Node {
		public int key;
		public int value;
		public Node next;
		public Node prev;

		public Node(int key, int value) {
			this.key = key;
			this.value = value;
		}

		@Override
		public String toString() {
			return this.key + " " + this.value;
		}
	}

	public static class LRUCache {

		private int capacity;
		private Node head;
		private Node tail;
		Map<Integer, Node> map = new HashMap<>();

		public LRUCache(int capacity) {
			this.capacity = capacity;
		}

		public void set(int key, int value) {
			if (map.containsKey(key)) {
				Node node = map.get(key);
				remove(node);
				setHead(node);
				node.value = value;
			} else {
				if (map.size() == capacity) {
					remove(tail);
					map.remove(tail.value);
				}
				Node created = new Node(key, value);
				setHead(created);
				map.put(key, created);
			}
		}

		public int get(int key) {
			if (map.containsKey(key)) {
				Node node = map.get(key);
				remove(node);
				setHead(node);
				return node.value;
			}
			return -1;
		}

		private void setHead(Node node) {
			if (head == null) {
				head = node;
				tail = node;
			} else {
				node.next = head;
				head.prev = node;
				head = node;
			}
		}

		private void remove(Node node) {
			if (node.prev != null) {
				node.prev.next = node.next;
			} else {
				head = node.next;
			}

			if (node.next != null) {
				node.next.prev = node.prev;
			} else {
				tail = node.prev;
			}
		}

		public void print() {
			System.out.println("Hash map");
			for (Map.Entry<Integer, Node> entry : map.entrySet()) {
				System.out.print("(" + entry.getKey() + ", " + entry.getValue() + ")");
			}
			System.out.println("\n------------------------------------");
			Node curr = head;
			System.out.println("Doubly Linked List");
			while (curr != null) {
				System.out.print(curr.value + " - > ");
				curr = curr.next;
			}
			System.out.println("\n====================================");
		}
	}

	public static void main(String[] args) {
		LRUCache lruCache = new LRUCache(3);
		lruCache.set(2, 1);
		lruCache.print();
		lruCache.set(2, 2);
		lruCache.print();
		lruCache.get(2);
		lruCache.print();
		lruCache.set(1, 1);
		lruCache.print();
		lruCache.set(4, 1);
		lruCache.print();
		lruCache.get(2);
		lruCache.print();
	}
}
