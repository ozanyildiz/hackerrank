package datastructures.trees;

public class Node {
	public int data;
	public Node left;
	public Node right;
}
