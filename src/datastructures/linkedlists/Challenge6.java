package datastructures.linkedlists;

// Print in Reverse
public class Challenge6 {
	void ReversePrint(Node head) {
		if (head == null) {
			return;
		}
		ReversePrint(head.next);
		System.out.println(head.data);
	}
}
