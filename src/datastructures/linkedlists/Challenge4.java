package datastructures.linkedlists;

// Insert a node at a specific position in a linked list
public class Challenge4 {
	Node InsertNth(Node head, int data, int position) {
		Node node = new Node();
		node.data = data;
		if (head == null) {
			return node;
		}
		if (position == 0) {
			node.next = head;
			return node;
		}
		Node ref = head;
		while ((position - 1) > 0) {
			ref = ref.next;
			--position;
		}
		node.next = ref.next;
		ref.next = node;
		return head;
	}

}
