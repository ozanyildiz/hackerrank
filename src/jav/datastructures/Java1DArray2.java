package jav.datastructures;

import java.util.Scanner;

public class Java1DArray2 {

	public static boolean doesWinGame(String[] s, int m, int currPos) {
		if (currPos < 0 || s[currPos].equals("1")) {
			return false;
		}
		if (currPos + 1 >= s.length || currPos + m >= s.length) {
			return true;
		}
		s[currPos] = "1";
		return doesWinGame(s, m, currPos + 1) || doesWinGame(s, m, currPos - 1) || doesWinGame(s, m, currPos + m);
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int t = in.nextInt();
		for (int i = 0; i < t; i++) {
			int n = in.nextInt();
			int m = in.nextInt();
			String[] s = new String[n];
			for (int a0 = 0; a0 < n; a0++) {
				s[a0] = in.next();
			}
			if (doesWinGame(s, m, 0)) {
				System.out.println("YES");
			} else {
				System.out.println("NO");
			}
		}
	}
}
