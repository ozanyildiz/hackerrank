package weekofcode25;

import java.util.Scanner;

public class BabyStepGiantStep {
	public static int babyGiantSteps(int a, int b, int d) {
		if (a == d)
			return 1;
		if (d % b == 0)
			return d / b;
		if (d > b) {
			int currStep = d / b;
			return currStep + 1;
		} else {
			return 2;
		}
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int s = in.nextInt();
		for (int a0 = 0; a0 < s; a0++) {
			int a = in.nextInt();
			int b = in.nextInt();
			int d = in.nextInt();
			System.out.println(babyGiantSteps(a, b, d));
		}
	}
}
