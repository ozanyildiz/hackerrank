package algorithms.sorting;

import java.util.ArrayList;
import java.util.Scanner;

public class TheFullCountingSort {
	public static ArrayList<ArrayList<String>> fillBuckets(int[] arr, String[] strings) {
		ArrayList<ArrayList<String>> buckets = initBuckets();
		for (int i = 0; i < arr.length; i++) {
			ArrayList<String> bucket = buckets.get(arr[i]);
			if (bucket == null) {
				bucket = new ArrayList<>();
			}
			bucket.add(strings[i]);
		}
		return buckets;
	}

	public static ArrayList<ArrayList<String>> initBuckets() {
		ArrayList<ArrayList<String>> buckets = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			ArrayList<String> bucket = new ArrayList<>();
			buckets.add(bucket);
		}
		return buckets;
	}

	public static void maskInput(String[] arr) {
		for (int i = 0; i < arr.length / 2; i++) {
			arr[i] = "-";
		}
	}

	public static void printArray(ArrayList<ArrayList<String>> buckets) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < buckets.size(); i++) {
			for (int j = 0; j < buckets.get(i).size(); j++) {
				sb.append(buckets.get(i).get(j));
				sb.append(" ");
			}
		}
		System.out.println(sb.toString());
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int s = in.nextInt();
		int[] ar = new int[s];
		String[] strings = new String[s];
		for (int i = 0; i < s; i++) {
			ar[i] = in.nextInt();
			strings[i] = in.next();
		}
		maskInput(strings);
		printArray(fillBuckets(ar, strings));
	}
}
