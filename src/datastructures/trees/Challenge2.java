package datastructures.trees;

//Tree: Postorder Traversal
public class Challenge2 {
	void postOrder(Node root) {
		if (root != null) {
			postOrder(root.left);
			postOrder(root.right);
			System.out.print(root.data + " ");
		}
	}
}
