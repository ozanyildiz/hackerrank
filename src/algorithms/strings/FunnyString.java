package algorithms.strings;

import java.util.Scanner;

public class FunnyString {

	public static void funnyOrNot(String s) {
		String r = new StringBuilder(s).reverse().toString();
		for (int i = 1; i < r.length() - 1; i++) {
			if (Math.abs(s.charAt(i) - s.charAt(i - 1)) != Math.abs(r.charAt(i) - r.charAt(i - 1))) {
				System.out.println("Not Funny");
				return;
			}
		}
		System.out.println("Funny");
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		for (int i = 0; i < n; i++) {
			String s = in.next();
			funnyOrNot(s);
		}
	}
}