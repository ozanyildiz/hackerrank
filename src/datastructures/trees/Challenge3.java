package datastructures.trees;

// Tree: Inorder Traversal
public class Challenge3 {
	void inOrder(Node root) {
		if (root != null) {
			System.out.println(root.data + " ");
			inOrder(root.left);
			inOrder(root.right);
		}
	}
}
