package datastructures.trees;

// Tree : Top View
public class Challenge5 {

	static void goLeft(Node root) {
		if (root != null) {
			goLeft(root.left);
			System.out.print(root.data + " ");
		}
	}
	
	static void goRight(Node root) {
		if (root != null) {
			System.out.print(root.data + " ");
			goRight(root.right);
		}
	}
	
	void top_view(Node root) {
		if (root != null) {
			goLeft(root.left);
			System.out.print(root.data + " ");
			goRight(root.right);
		}
	}
}
