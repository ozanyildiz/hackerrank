package implementation;

import java.util.Scanner;

public class CavityMap {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		String grid[] = new String[n];
		for (int grid_i = 0; grid_i < n; grid_i++) {
			grid[grid_i] = in.next();
		}

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i - 1 >= 0 && i + 1 < n
						&& Integer.valueOf(grid[i].charAt(j)) > Integer.valueOf(grid[i - 1].charAt(j))
						&& Integer.valueOf(grid[i].charAt(j)) > Integer.valueOf(grid[i + 1].charAt(j)) && j - 1 >= 0
						&& j + 1 < n && Integer.valueOf(grid[i].charAt(j)) > Integer.valueOf(grid[i].charAt(j - 1))
						&& Integer.valueOf(grid[i].charAt(j)) > Integer.valueOf(grid[i].charAt(j + 1))) {
					System.out.print("X");
				} else {
					System.out.print(grid[i].charAt(j));
				}
			}
			System.out.println();
		}
	}
}
